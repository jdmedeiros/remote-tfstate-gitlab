terraform {
  required_version = ">= 0.13"
}

resource "aws_instance" "emailsrv" {
  ami = "ami-0885b1f6bd170450c"
  instance_type = "t2.micro"
  key_name = "AWS-KEY-PAIR"
  security_groups = [
    "email-security-group",
  ]
  tags = {
    "Ano" = "2020"
    "Curso" = "GRSI"
    "Name" = "emailsrv"
  }
  volume_tags = {
    "Ano" = "2020"
    "Curso" = "GRSI"
  }
  vpc_security_group_ids = [
    aws_security_group.instance.id,
  ]
  root_block_device {
    volume_size = 28
  }
  user_data = data.template_cloudinit_config.config.rendered

}

# aws_security_group.instance:
resource "aws_security_group" "instance" {
  description = "EMAIL Security Group"
  egress = [
    {
      cidr_blocks = [
        "0.0.0.0/0",
      ]
      description = ""
      from_port = 0
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      protocol = "-1"
      security_groups = []
      self = false
      to_port = 0
    },
  ]
  ingress = [
    for _port in var.port_list:
    {
      cidr_blocks = [
      for _ip in var.ip_list:
      _ip
      ]
      description = ""
      from_port = _port
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      protocol = "tcp"
      security_groups = []
      self = false
      to_port = _port
    }
  ]
  name = "email-security-group"
}