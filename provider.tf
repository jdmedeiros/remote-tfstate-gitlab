terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.22.0"
    }
    cloudinit = {
      source = "hashicorp/cloudinit"
      version = "2.1.0"
    }
  }
  backend "s3" {
    bucket = "jdmedeiros-001-tfstate"
    key = "global/s3/terraform.tfstate"
    region = "us-east-1"
    # Replace this with your DynamoDB table name!
    dynamodb_table = "jdmedeiros-001-tfstate-locks"
    encrypt = true
  }
}

provider "aws" {
  region = "us-east-1"
}

provider "cloudinit" {
}

data "template_cloudinit_config" "config" {
  gzip = true
  base64_encode = true

  part {
    filename = var.variables
    content_type = "text/x-shellscript"
    content = file(var.variables)
  }

  part {
    filename = var.certificates
    content_type = "text/x-shellscript"
    content = file(var.certificates)
  }

  part {
    filename = var.cloud_config
    content_type = "text/x-shellscript"
    content = file(var.cloud_config)
  }

}

