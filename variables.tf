variable "tfstate_bucket_name" {
  description = "The name of the S3 bucket. Must be globally unique."
  type        = string
  default     = "jdmedeiros-001-tfstate"
}

variable "tfstate_table_name" {
  description = "The name of the DynamoDB table. Must be unique in this AWS account."
  type        = string
  default     = "jdmedeiros-001-tfstate-locks"
}


variable "variables" {
  default = "variables.sh"
}

variable "certificates" {
  default = "certificates.sh"
}

variable "cloud_config" {
  default = "cloud-config.sh"
}

variable "create-user" {
  default = "create-user.sh"
}

variable "ip_list" {
  description = "Allowed IPs"
  type = list(string)
  default = [
    "10.0.0.0/8",
    "172.16.0.0/12",
    "192.168.0.0/16",
    "128.65.243.205/32",
  ]
}

variable "port_list" {
  description = "Allowed ports"
  type = list(number)
  default = [
    22,
    25,
    110,
    143,
    465,
    587,
    993,
    995,
    1025,
    8888
  ]
}
