#!/bin/bash -x

if [ "$1" = "run" ]
then
SCRIPT_LOG_DETAIL=/var/log/cloud-config-detail.log

# Reference: https://serverfault.com/questions/103501/how-can-i-fully-log-all-bash-scripts-actions
# Log everything
#
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>$SCRIPT_LOG_DETAIL 2>&1

USER=maria
PASSWORD=Passw0rd
MAILHOST=grsi.pt
CERTSRV=true
CERTSRVPORT=8888
fi
