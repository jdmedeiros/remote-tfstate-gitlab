#!/bin/bash -x
if [ "$1" = "run" ]; then
  CURRENTDIR=$(pwd)
  apt-get update && apt-get -y upgrade && apt-get -y install ssl-cert

  cd /etc/
  git clone https://github.com/OpenVPN/easy-rsa.git
  cd /etc/easy-rsa/easyrsa3
  ./easyrsa init-pki

  ./easyrsa \
    --batch \
    --dn-mode=org \
    --req-c=PT \
    --req-st=Azores \
    --req-city="Ponta Delgada" \
    --req-org="Entidade Certificadora Nacional" \
    --req-email=webmaster@entcert.pt \
    --req-ou="Departamento de Certificados" \
    --req-cn=www.entacert.pt \
    build-ca nopass

  ./easyrsa \
    --batch \
    --dn-mode=org \
    --req-c=PT \
    --req-st=Azores \
    --req-city="Ponta Delgada" \
    --req-org="ENTA - Escola de Novas Tecnologias" \
    --req-email=webmaster@enta.pt \
    --req-ou="Departamento de Informatica" \
    --req-cn="*.amazonaws.com" \
    --subject-alt-name="DNS:*.amazonaws.com" \
    build-server-full amazonaws.com nopass

  cp pki/ca.crt /etc/ssl/certs
  chmod +r /etc/ssl/certs/ca.crt
  cp pki/issued/amazonaws.com.crt /etc/ssl/certs
  chmod +r /etc/ssl/certs/amazonaws.com.crt
  cp pki/private/ca.key /etc/ssl/private
  chmod g+r /etc/ssl/private/ca.key
  chgrp ssl-cert /etc/ssl/private/ca.key
  cp pki/private/amazonaws.com.key /etc/ssl/private
  chmod g+r /etc/ssl/private/amazonaws.com.key
  chgrp ssl-cert /etc/ssl/private/amazonaws.com.key

  if [ "$CERTSRV" = "true" ]; then
    mkdir /tmp/certs
    cp /etc/ssl/certs/ca.crt /tmp/certs
    cp /etc/ssl/certs/amazonaws.com.crt /tmp/certs
    chmod -R +r /tmp/certs
    cd /tmp/certs
    nohup python3 -m http.server $CERTSRVPORT  >/dev/null 2>&1 &
  fi
  cd $CURRENTDIR
fi
